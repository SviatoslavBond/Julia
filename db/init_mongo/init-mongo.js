db = db.getSiblingDB("admin");
db.auth("admin", "admin");

var kulokDb = db.getSiblingDB(process.env.MONGO_INITDB_DATABASE);

kulokDb.createUser({
  user: process.env.KULOK_DB_USERNAME,
  pwd: process.env.KULOK_DB_PASSWORD,
  roles: [{ role: "dbOwner", db: process.env.MONGO_INITDB_DATABASE }],
});
kulokDb.createCollection("stories");
