VERSION_TXT=$(cat version.txt)
docker build -t registry.gitlab.com/sviatoslavbond/julia:$VERSION_TXT . --file ./Dockerfile.server
docker tag registry.gitlab.com/sviatoslavbond/julia:$VERSION_TXT registry.gitlab.com/sviatoslavbond/julia:latest
docker push registry.gitlab.com/sviatoslavbond/julia:$VERSION_TXT
docker push registry.gitlab.com/sviatoslavbond/julia:latest