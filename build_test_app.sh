
docker build -t registry.gitlab.com/sviatoslavbond/julia:latest-test . --file ./Dockerfile.server
docker push registry.gitlab.com/sviatoslavbond/julia:latest-test

docker build -t registry.gitlab.com/sviatoslavbond/julia:client-latest-test . --file ./Dockerfile.test.client
docker push registry.gitlab.com/sviatoslavbond/julia:client-latest-test