import express from "express";
import {
  storyCreateValidation,
  mongoObjectIdValidationInParams,
  getStoriesValidation,
} from "../middleware/validators/validations.js";
import handleValidationErrors from "../middleware/validators/handleValidationErrors.js";
import { stories } from "../controllers/story.controller.js";
import { authRequired } from "../middleware/auth.middleware.js";
import { multerGCS } from "../middleware/multer.js";
import { filesValidator } from "../middleware/validators/filesValidator.js";
const storyRouter = express.Router();

/**
 * @swagger
 * /api/stories:
 *  post:
 *    tags:
 *    - Stories
 *    summary: Creating new story
 *    description: Creating new story. It involvs saving to DB , moving files from   <b>temporary</b> folder
 *                to <b>persist directory</b> on the GSC.
 *    requestBody:
 *      description: Body will be validate therefore passed fields must be in body object
 *      required: true
 *      content:
 *        multipart/form-data:
 *          schema:
 *            $ref: '#/components/requestBodies/CreateStory'
 *    responses:
 *      200:
 *        description: Create story successfully
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/200_CreateStory'
 *      400:
 *        description: Bad request
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/400_CreateStory'
 *      401:
 *        description: Unathorized
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/401_Unathorized_error'
 *    security:
 *      - accessToken: []
 */
storyRouter.post(
  "/",
  authRequired,
  multerGCS.array("photo[]"),
  filesValidator,
  storyCreateValidation,
  handleValidationErrors,
  stories.createStory
);
/**
 * @swagger
 * /api/stories:
 *  get:
 *    tags:
 *    - Stories
 *    summary: Get stories by category or all
 *    description: Get  all stories or sorted by category when query category was passed.
 *        If query parametr wasn't passed then all stories are being returned
 *    parameters:
 *      - in: query
 *        description: String values which will be used for getting stories sorted by category
 *        name: category
 *        required: false
 *        schema:
 *          type: string
 *          enum:
 *            - wedding
 *            - family
 *            - lovestory
 *            - content
 *    responses:
 *      200:
 *        description: Stories was gotten and send to client
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/200_GetStories'
 */
storyRouter.get(
  "/",
  getStoriesValidation,
  handleValidationErrors,
  stories.getSrories
);
/**
 * @swagger
 * /api/stories/{id}:
 *  get:
 *    tags:
 *    - Stories
 *    summary: Get story by id
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: string
 *          example: '65a001301a85047822dc3b43'
 *    responses:
 *      200:
 *        description: Story was successfully found
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/200_CreateStory'
 *      400:
 *        description: Bad request
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/400_InvalidMongooseId'
 *      404:
 *        description: Not found
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/404_GetById'
 */
storyRouter.get(
  "/:id",
  mongoObjectIdValidationInParams,
  handleValidationErrors,
  stories.getOne
);

/**
 * @swagger
 * /api/stories/{id}:
 *  delete:
 *    tags:
 *    - Stories
 *    summary: Delete story by id
 *    description: Deleting story both from DB adn google cloud <b>Warning this method is deleting story permanently </b>
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: string
 *          example: '65a001301a85047822dc3b43'
 *    responses:
 *      200:
 *        description: Story was successfully deleted
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/200_DeleteStory'
 *      400:
 *        description: Bad request
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/400_InvalidMongooseId'
 *      404:
 *        description: Not found
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/404_GetById'
 *    security:
 *      - accessToken: []
 */
storyRouter.delete(
  "/:id",
  authRequired,
  mongoObjectIdValidationInParams,
  handleValidationErrors,
  stories.deleteOne
);

export { storyRouter };
