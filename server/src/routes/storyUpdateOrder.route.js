import express from "express";
import { StoryEditController } from "../controllers/storyEdit.controller.js";
import { authRequired } from "../middleware/auth.middleware.js";
const storyUpdateOrderRouter = express.Router();
/**
 * @swagger
 * /api/edit/update-order:
 *  patch:
 *    tags:
 *    - Update story
 *    summary: Change story order
 *    description: Change story order by passing array of IDs
 *    requestBody:
 *      description: Body must have field storyIDs
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/requestBodies/UpdateOrder'
 *
 *
 *    responses:
 *      200:
 *        description: Order of stories successfully has been
 *      401:
 *        description: Unathorized
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/401_Unathorized_error'
 *    security:
 *      - accessToken: []
 */
storyUpdateOrderRouter.patch(
  "/",
  authRequired,
  StoryEditController.updateStoryOrder
);

export { storyUpdateOrderRouter };
