import express from "express";
import { StoryEditController } from "../controllers/storyEdit.controller.js";
import { authRequired } from "../middleware/auth.middleware.js";
import { multerGCS } from "../middleware/multer.js";
import { filesValidator } from "../middleware/validators/filesValidator.js";
import {
  storyUpdateValidation,
  mongoObjectIdValidationInParams,
} from "../middleware/validators/validations.js";
import handleValidationErrors from "../middleware/validators/handleValidationErrors.js";
const storyEditRouter = express.Router();

/**
 * @swagger
 * /api/edit/story/{id}:
 *  delete:
 *    tags:
 *    - EditStory
 *    summary: Deleting one image from existing story
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: string
 *          example: '65a001301a85047822dc3b43'
 *    description: Deleting one image from existing story. This includes deleting both from db and cloud.
 *    requestBody:
 *      description: Body will be validate therefore passed field must be in body object
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            type: object
 *            properties:
 *              filename:
 *                type: string
 *                example: "family/85d7c022-593f-4151-bb60-d3153ab0e3ff/fac2d875-6997-4429-b0f5-bee66fe79cd9_2.webp"
 *    responses:
 *      200:
 *        description: Story was successfully edited
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/200_CreateStory'
 *      401:
 *        description: Unathorized
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/401_Unathorized_error'
 *      400:
 *        description: Invalid ObjectId. Check id parametr in path it's must valid ObjectId
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/400_InvalidMongooseId'
 *      404:
 *        description: Story or file which you intend delete wasn't found
 *                      Check you passed correct filename and appropriate story ID
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/404_GetById'
 *    security:
 *      - accessToken: []
 */
storyEditRouter.delete(
  "/:id",
  authRequired,
  mongoObjectIdValidationInParams,
  handleValidationErrors,
  StoryEditController.deleteOneImageFromStory
);

/**
 * @swagger
 * /api/edit/story/:
 *  put:
 *    tags:
 *    - EditStory
 *    summary: Updating the existence story
 *    description: Updating story requires passing full story object with updated fields
 *    requestBody:
 *      description: Body will be validate therefore passed field must be in body object
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/responses/200_CreateStory'
 *    responses:
 *      200:
 *        description: Story was successfully edited
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/200_CreateStory'
 *      400:
 *        description: Invalid ObjectId. Check id parametr in path it's must valid ObjectId
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/400_InvalidMongooseId'
 *      401:
 *        description: Unathorized
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/401_Unathorized_error'
 *    security:
 *      - accessToken: []
 */
storyEditRouter.put(
  "/",
  authRequired,
  storyUpdateValidation,
  handleValidationErrors,
  StoryEditController.updateOne
);
/**
 * @swagger
 * /api/edit/story/{id}:
 *  patch:
 *    tags:
 *    - EditStory
 *    summary: Add new photos to story
 *    parameters:
 *      - in: path
 *        name: id
 *        required: true
 *        schema:
 *          type: string
 *          example: '65a001301a85047822dc3b43'
 *    description: Add bunch of photo
 *    requestBody:
 *      description: Body will be validate therefore passed field must be in body object
 *      required: true
 *      content:
 *        multipart/form-data:
 *          schema:
 *            type: object
 *            properties:
 *              filename:
 *                type: string
 *                example: "family/85d7c022-593f-4151-bb60-d3153ab0e3ff/fac2d875-6997-4429-b0f5-bee66fe79cd9_2.webp"
 *              photo:
 *                type: array
 *                items:
 *                  type: string
 *                  format: binary
 *
 *    responses:
 *      200:
 *        description: Story was successfully edited
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/200_CreateStory'
 *      401:
 *        description: Unathorized
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/401_Unathorized_error'
 *      400:
 *        description: Invalid ObjectId. Check id parametr in path it's must valid ObjectId
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/400_InvalidMongooseId'
 *      404:
 *        description: Story or file which you intend delete wasn't found
 *                      Check you passed correct filename and appropriate story ID
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/404_GetById'
 *    security:
 *      - accessToken: []
 */
storyEditRouter.patch(
  "/:id",
  authRequired,
  mongoObjectIdValidationInParams,
  multerGCS.array("photo[]"),
  filesValidator,
  handleValidationErrors,
  StoryEditController.addNewPhotos
);
export { storyEditRouter };
