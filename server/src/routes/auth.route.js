//@ts-check
import { Router } from "express";
import { userController } from "../controllers/user.controller.js";
import { userCreateValidation } from "../middleware/validators/validations.js";
import handleValidationErrors from "../middleware/validators/handleValidationErrors.js";
const authRouter = Router();
/**
 * @swagger
 * /api/auth/registration:
 *  post:
 *    tags:
 *    - Auth
 *    summary: Admin registration
 *    description: Only 1 admin is allowed to be registrated .
 *    requestBody:
 *      description: Providing email and password is required
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/requestBodies/Registration'
 *    responses:
 *      200:
 *        description: Registration successfully done
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/200_Registration'
 *      400:
 *        description: Bad request
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/400_Registration'
 */
authRouter.post(
  "/registration",
  userCreateValidation,
  handleValidationErrors,
  userController.registration
);
/**
 * @swagger
 * /api/auth/login:
 *  post:
 *    tags:
 *    - Auth
 *    summary: Admin login
 *    description: For managing stories you must be loged
 *    requestBody:
 *      description: Providing email and password is required
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/requestBodies/Registration'
 *    responses:
 *      200:
 *        description: Log in successfully done
 *        headers:
 *          Set-Cookie:
 *            schema:
 *              type: string
 *              example: refreshToken=abcde12345; Path=/; HttpOnly
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/200_Registration'
 *      400:
 *        description: Bad request
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/400_Login'
 */
authRouter.post(
  "/login",
  userCreateValidation,
  handleValidationErrors,
  userController.login
);
/**
 * @swagger
 * /api/auth/logout:
 *  post:
 *    tags:
 *    - Auth
 *    summary: Admin logout
 *    description: Logout
 *    parameters:
 *      - in: cookie
 *        name: refreshToken
 *        schema:
 *          type: string
 *          example: 'adasdasdqweqw13..12312'
 *    responses:
 *      200:
 *        description: Logout in successfully done
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/200_Logout'
 */
authRouter.post("/logout", userController.logout);
/**
 * @swagger
 * /api/auth/activate/{link}:
 *  get:
 *    tags:
 *    - Auth
 *    summary: Activate registration link
 *    description: Activate link
 *    parameters:
 *      - in: path
 *        name: link
 *        required: true
 *        schema:
 *          type: string
 *          example: 'e719548c-f5b8-4783-9d55-51f4aa0bac95'
 *    responses:
 *      200:
 *        description: After success activation user is being redirected to frontend page
 */
authRouter.get("/activate/:link", userController.activate);
/**
 * @swagger
 * /api/auth/refreshToken:
 *  get:
 *    tags:
 *    - Auth
 *    summary: Refresh access token
 *    description: For refreshing refresh token is required
 *    parameters:
 *      - in: cookie
 *        name: refreshToken
 *        required: true
 *        schema:
 *          type: string
 *          example: 'JhbGciOiJIUzI1NiIsInR5cCI6...'
 *    responses:
 *      200:
 *        description: Refreshing token has done
 *        headers:
 *          Set-Cookie:
 *            schema:
 *              type: string
 *              example: refreshToken=abcde12345; Path=/; HttpOnly
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/200_Registration'
 */
authRouter.get("/refresh", userController.refreshToken);
/**
 * @swagger
 * /api/auth:
 *  get:
 *    tags:
 *    - Auth
 *    summary: Get admin data
 *    responses:
 *      200:
 *        description: Getting admin was successfull
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/200_Registration'
 */
authRouter.get("/", userController.getAdmin);
export { authRouter };
