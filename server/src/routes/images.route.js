import express from "express";
import { cloudController } from "../controllers/cloudStorage.controller.js";
import { multerGCS } from "../middleware/multer.js";
import { authRequired } from "../middleware/auth.middleware.js";
import { filesValidator } from "../middleware/validators/filesValidator.js";
const imageRouter = express.Router();

/**
 * @swagger
 * /api/images/:
 *  post:
 *    tags:
 *    - Images
 *    summary: Uploading story images to specifed folder
 *    description: Uploading images to <b>specifed</b> folder on google cloud .
 *              This route only for uploading images
 *    requestBody:
 *      description: Providing images is required (input field must have called - <b> "photo"</b>)
 *      required: true
 *      content:
 *        multipart/form-data:
 *          schema:
 *            $ref: '#/components/requestBodies/UploadPhotoBody'
 *    responses:
 *      200:
 *        description: Upload story photo
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/UploadPhotoResponse'
 *      400:
 *        description: Bad request
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/400_BadUploadRequest'
 *      401:
 *        description: Unathorized
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/401_Unathorized_error'
 *    security:
 *      - accessToken: []
 */

imageRouter.post(
  "/",
  authRequired,
  multerGCS.array("photo"),
  filesValidator,
  cloudController.uploadFiles
);
/**
 * @swagger
 * /api/images/:
 *  delete:
 *    tags:
 *    - Images
 *    summary: Delete one image
 *    description: Delete image from google cloud
 *    requestBody:
 *      description: Make sure that you passed correct - <b> filename </b>
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/requestBodies/DeleteFile'
 *    responses:
 *      200:
 *        description: File deleted successfully
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/200_deleted_file'
 *      404:
 *        description: Not Found
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/404_deleted_file'
 *      401:
 *        description: Unathorized
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/401_Unathorized_error'
 *    security:
 *      - accessToken: []
 *
 */
imageRouter.delete("/", authRequired, cloudController.deleteOne);

/**
 * @swagger
 * /api/images/deleteMany:
 *  delete:
 *    tags:
 *    - Images
 *    summary: Delete many images
 *    description: Delete images from google cloud
 *    requestBody:
 *      description: Make sure that you passed correct - <b> prefixPath </b>
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/requestBodies/DeleteManyFiles'
 *    responses:
 *      200:
 *        description: Files deleted successfully
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/200_deleted_file'
 *      500:
 *        description: Internal error
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/500_Internal_error'
 *      401:
 *        description: Unathorized
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/responses/401_Unathorized_error'
 *    security:
 *      - accessToken: []
 */
imageRouter.delete("/deleteMany", authRequired, cloudController.deleteMany);

export { imageRouter };
