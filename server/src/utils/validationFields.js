export const VALID_FIELDS_CREATE_STORY = {
  storyName: {
    name: "storyName",
    msg: "Enter storyName of story",
  },
  location: {
    name: "location",
    msg: "Enter location of story",
  },
  category: {
    name: "category",
    msg: "Category is required. Field should contain one of those - wedding, content, family, engagement",
  },
  describe: {
    name: "describe",
    msg: "Describe is required",
  },
  title: {
    name: "title",
    msg: "Enter title of story",
  },
  date: {
    name: "date",
    msg: "You did not type the date",
  },
  photoData: {
    name: "photoData",
    msg: "PhotoData must be array",
  },
};
export const VALID_FIELDS_UPDATE_STORY = {};
