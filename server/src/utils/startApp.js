import mongoose from "mongoose";
import { runMigration } from "../migration/runMigration.js";
const clientOptions = {
  serverApi: { version: "1", strict: true, deprecationErrors: true },
};
export const startApp = (app, PORT) => {
  mongoose
    .connect(process.env.MONGO_URI, clientOptions)
    .then(async (client) => {
      console.log(`Succesful conection to DB`);
      await runMigration();
    })
    .then(() => {
      app.listen(PORT, (err) => {
        if (err) {
          console.log(`Server started with error ${err}`);
        } else {
          console.log(`Server started port > ${PORT}`);
        }
      });
    })
    .catch((err) => {
      console.log(`DB conection error ${err}`);
    });
};
