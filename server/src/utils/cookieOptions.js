const refreshTokenLife = +process.env.LIFE_TIME_REFRESH_TOKEN.replace(
  /[a-zA-Z]+/g,
  ""
);
const oneDayInMilliseconds = 24 * 60 * 60 * 1000;
const lifeTimeOfCookie = refreshTokenLife * oneDayInMilliseconds;

const cookieOptionsProd = {
  maxAge: lifeTimeOfCookie,
  httpOnly: true,
  sameSite: "none",
  secure: true,
};
const cookieOptionsDev = {
  maxAge: lifeTimeOfCookie,
  httpOnly: true,
};

const cookieOptions = process.env.DEVELOPMENT_MODE
  ? cookieOptionsDev
  : cookieOptionsProd;
export { cookieOptions };
