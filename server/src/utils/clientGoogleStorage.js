// @ts-check
import { Storage } from "@google-cloud/storage";
import path from "path";
import { dirname } from "../dirname.js";

const storage = new Storage({
  keyFilename: path.join(dirname, "../my-key.json"),
  projectId: process.env.GOOGLE_PROJECT_ID,
});

const bucket = storage.bucket(
  process.env.GOOGLE_BUCKET_NAME || "no_env_bucket_name"
);
export {bucket}
