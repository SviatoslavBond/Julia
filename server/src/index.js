import "dotenv/config";
import express from "express";
import cors from "cors";
import cookieParser from "cookie-parser";
import { startApp } from "./utils/startApp.js";
import { storyEditRouter } from "./routes/storyEdit.route.js";
import { storyRouter } from "./routes/stories.route.js";
import { imageRouter } from "./routes/images.route.js";
import { authRouter } from "./routes/auth.route.js";
import { storyUpdateOrderRouter } from "./routes/storyUpdateOrder.route.js";
import { handleEror404 } from "./middleware/404.js";
import { dirname } from "./dirname.js";
import { swaggerDocs } from "./swagger/swagger.js";
import { errorMidleware } from "./middleware/error.midleware.js";
const PORT = process.env.PORT || 5000;

const app = express();

const allowedOrigins = process.env.ALLOWED_CLIENT.split(",").map((url) =>
  url.replace(/\s/g, "")
);
app.use(express.json());
app.use(cookieParser());
app.use(
  cors({
    credentials: true,
    origin: allowedOrigins,
  })
);

app.use(express.static(dirname));

app.use("/api/stories", storyRouter);
app.use("/api/images", imageRouter);
app.use("/api/auth", authRouter);
app.use("/api/edit/story", storyEditRouter);
app.use("/api/edit/update-order", storyUpdateOrderRouter);

if (process.env.DEVELOPMENT_MODE) {
  swaggerDocs(app, PORT);
}

app.use(handleEror404);
app.use(errorMidleware);

startApp(app, PORT);
