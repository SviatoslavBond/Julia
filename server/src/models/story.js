//@ts-check
import { Schema, model } from "mongoose";
const StorySchema = new Schema(
  {
    storyName: { type: String, required: true },
    describe: { type: String },
    category: {
      name: { type: String, required: true },
      categoryOrder: { type: Number, default: 0, require: true },
    },
    location: { type: String, required: true },
    date: { type: String, required: true },
    title: { type: String, required: true },
    prefixPath: { type: String, required: true },
    numberOfPhoto: { type: Number, required: true },
    photo: [
      {
        cover: { type: Boolean, required: true },
        publicUrl: { type: String, required: true },
        filename: { type: String, required: true },
        mimeType: { type: String, default: "" },
        size: { type: Number, default: 0 },
        originalName: { type: String, default: "" },
      },
    ],
    order: { type: Number, default: 0, require: true },
  },
  { timestamps: true }
);
export const StoryModel = model("Story", StorySchema);
