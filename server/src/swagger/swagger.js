//@ts-check
import swaggerUi from "swagger-ui-express";
import swaggerJsdoc from "swagger-jsdoc";

const options = {
  definition: {
    openapi: "3.0.0",
    info: {
      title: "Julia kulok API",
      version: "1.0.0",
      description:
        "This is  CRUD API application made with Express and documented with Swagger",
      license: {
        name: "MIT",
        url: "https://spdx.org/licenses/MIT.html",
      },
      contact: {
        name: "Julia Kulok",
        email: "gigantos773@gmail.com",
      },
    },
    servers: [
      {
        url: "http://localhost:8089",
        description: "Development server",
      },
    ],
    tags: [
      {
        name: "Auth",
        description: "Authentication, login, logout, refreshing ",
      },
      { name: "Images", description: "All about processing images" },
      {
        name: "Stories",
        description: "Managing your stories, adding, deleting ...",
      },
      {
        name: "EditStory",
        description: "All about editing story  ...",
      },
    ],
  },
  apis: ["./src/routes/*.js", "./src/swagger/SwaggerSchema/*.yml"],
};
//@ts-ignore
const swaggerSpec = swaggerJsdoc(options);

/**
 * @param {*} app
 * @param {number} port
 */
function swaggerDocs(app, port) {
  app.use("/swagger", swaggerUi.serve, swaggerUi.setup(swaggerSpec));
  console.log("swagger started");
}

export { swaggerDocs };
