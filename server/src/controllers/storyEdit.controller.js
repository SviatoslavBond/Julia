//@ts-check
import { StoryEditService } from "../services/storyEdit.service.js";
import { fileService } from "../services/images.service.js";
import { storyService } from "../services/story.service.js";
import express from "express";
import { ApiError } from "../exeptions/api.error.js";

class StoryEditController {
  /**
   *
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */
  static async deleteOneImageFromStory(req, res, next) {
    try {
      const storyId = req.params.id;
      const { filename } = req.body;

      await storyService.checkStoryExist(storyId);

      await fileService.deleteOneFile(filename);
      const result = await StoryEditService.deleteOneImageFromStory(
        storyId,
        filename
      );
      res.json(result);
    } catch (e) {
      next(e);
    }
  }

  /**
   *
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */
  static async updateOne(req, res, next) {
    try {
      const newStory = req.body;

      const story = await StoryEditService.updateOne(newStory);

      res.json(story);
    } catch (e) {
      next(e);
    }
  }
  /**
   *
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */
  static async addNewPhotos(req, res, next) {
    try {
      const storyId = req.params.id;
      const { folder } = req.body;
      await storyService.checkStoryExist(storyId);
      if (!folder) {
        throw ApiError.BadRequest("Field folder should be passed");
      }
      const savedPhotos = await fileService.uploadFiles(req.files, folder);
      const story = await StoryEditService.addPhotoToStory(
        storyId,
        savedPhotos.images
      );

      res.json(story);
    } catch (e) {
      next(e);
    }
  }
  /**
   *
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */
  static async updateStoryOrder(req, res, next) {
    try {
      const { storyIDs } = req.body;
      const response = await StoryEditService.updateStoryOrder(storyIDs);
      res.json(response);
    } catch (e) {
      next(e);
    }
  }
}
export { StoryEditController };
