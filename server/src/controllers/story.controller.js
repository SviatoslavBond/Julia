// @ts-check
import { StoryModel } from "../models/story.js";
import { storyService } from "../services/story.service.js";
import { ApiError } from "../exeptions/api.error.js";
import express from "express";

class Stories {
  /**
   *
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */
  async createStory(req, res, next) {
    try {
      const story = await storyService.createStory(req.body, req.files);
      res.json(story);
    } catch (e) {
      next(e);
    }
  }
  /**
   *
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */
  async getSrories(req, res, next) {
    try {
      const category = req.query.category;
      let stories = [];
      if (!category) {
        stories = await StoryModel.find();
      } else {
        stories = await StoryModel.find({ "category.name": category }).sort(
          "category.categoryOrder"
        );
      }

      res.json(stories);
    } catch (e) {
      next(e);
    }
  }
  /**
   *
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */
  async getOne(req, res, next) {
    try {
      const storyId = req.params.id;
      const story = await storyService.checkStoryExist(storyId);
      res.json(story);
    } catch (e) {
      next(e);
    }
  }
  /**
   *
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */
  async deleteOne(req, res, next) {
    try {
      const storyId = req.params.id;
      const result = await storyService.deleteOne(storyId);
      res.json(result);
    } catch (e) {
      next(e);
    }
  }
}

const stories = new Stories();
export { stories };
