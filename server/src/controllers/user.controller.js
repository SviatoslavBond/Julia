import { userService } from "../services/user.service.js";
import express from "express";
import { cookieOptions } from "../utils/cookieOptions.js";

class UserController {
  /**
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */
  async registration(req, res, next) {
    try {
      const { email, password } = req.body;
      const user = await userService.registration(email, password);
      res.cookie("refreshToken", user.refreshToken, cookieOptions);
      return res.json(user);
    } catch (e) {
      next(e);
    }
  }
  // ------------------------------------------------------------------------
  /**
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */
  async activate(req, res, next) {
    try {
      const activationLink = req.params.link;
      await userService.activateLink(activationLink);
      return res.redirect(process.env.CLIENT_URL);
    } catch (e) {
      next(e);
    }
  }
  // ------------------------------------------------------------------------
  /**
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */
  async login(req, res, next) {
    try {
      const { email, password } = req.body;
      const adminData = await userService.login(email, password);
      res.cookie("refreshToken", adminData.refreshToken, cookieOptions);
      return res.json(adminData);
    } catch (e) {
      next(e);
    }
  }
  // ------------------------------------------------------------------------
  /**
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */
  async logout(req, res, next) {
    try {
      const { refreshToken } = req.cookies;
      const token = await userService.logout(refreshToken);
      res.clearCookie("refreshToken");
      return res.json(token);
    } catch (e) {
      next(e);
    }
  }
  // ------------------------------------------------------------------------
  /**
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */
  async refreshToken(req, res, next) {
    try {
      const { refreshToken } = req.cookies;

      const admin = await userService.refreshToken(refreshToken);

      res.cookie("refreshToken", admin.refreshToken, cookieOptions);
      return res.json(admin);
    } catch (e) {
      next(e);
    }
  }
  // ------------------------------------------------------------------------
  /**
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */

  async getAdmin(req, res, next) {
    try {
      const admin = await userService.getAdmin();
      return res.json(admin);
    } catch (e) {
      next(e);
    }
  }
}

export const userController = new UserController();
