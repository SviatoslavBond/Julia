// @ts-check
import { fileService } from "../services/images.service.js";
import { ApiError } from "../exeptions/api.error.js";
import express from "express";
class CloudStorageController {
  /**
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */
  async uploadFiles(req, res, next) {
    try {
      const { folder } = req.body;
      if (!folder) {
        throw ApiError.BadRequest(`Field folder must be passed`);
      }
      const result = await fileService.uploadFiles(req.files, folder);
      res.json(result);
    } catch (error) {
      next(error);
    }
  }
  /**
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */
  async deleteOne(req, res, next) {
    try {
      const { filename } = req.body;
      if (!filename) {
        throw ApiError.BadRequest("Filename field is required");
      }
      await fileService.deleteOneFile(filename);
      res.json({ msg: "File deleted successfully", filename });
    } catch (error) {
      if (Object.keys(error).length === 0) {
        res.status(500).json({
          error,
          msg: "Internal error may be make sure that field filiname is correct",
        });
      } else {
        next(ApiError.NotFound(error.message, [error]));
      }
    }
  }
  /**
   * @param {express.Request} req
   * @param {express.Response} res
   * @param {express.NextFunction} next
   */
  async deleteMany(req, res, next) {
    try {
      const { prefixPath } = req.body;
      await fileService.deleteFiles(prefixPath);
      res.json({
        msg: "Deleting was performed",
        prefixPath,
        comparePassedPath: true,
      });
    } catch (error) {
      res.status(500).json(error);
    }
  }
}
const cloudController = new CloudStorageController();
export { cloudController };
