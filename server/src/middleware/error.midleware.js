import { ApiError } from "../exeptions/api.error.js";
import { ApiError as GooggleApiError } from "@google-cloud/storage";
import { MulterError } from "multer";
import mongoose from "mongoose";
export function errorMidleware(err, req, res, next) {
  console.log({
    errorCatchedInMidleWare: err,
  });

  if (err instanceof ApiError) {
    return res
      .status(err.status)
      .json({ message: err.message, errors: err.errors });
  }
  if (err instanceof mongoose.Error) {
    return res.status(500).json(err);
  }
  if (err instanceof MulterError) {
    return res.status(400).json({
      invalidField: err.field,
      message: err.message,
      statusCode: err.code,
      errorName: err.name,
    });
  }
  if (err instanceof GooggleApiError) {
    return res
      .status(err.code)
      .json({ errors: err.errors, message: err.message, statusCode: err.code });
  }
  return res.status(500).json({ msg: "Unknown error... ops", err });
}
