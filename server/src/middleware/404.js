export const handleEror404 = (req, res) => {
  res.status(404).json({
    error: {
      name: "Error",
      status: 404,
      message: "Invalid Request",
      statusCode: 404,
    },
  });
};
