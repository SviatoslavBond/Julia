import { ApiError } from "../exeptions/api.error.js";
import { tokenService } from "../services/token.service.js";
const authRequired = (req, res, next) => {
  try {
    const authorizationHeader = req.headers.authorization;
    if (!authorizationHeader) {
      return next(
        ApiError.UnauthorizedError("No token in authorizationHeader")
      );
    }
    const token = authorizationHeader.split(" ")[1];
    if (!token) {
      return next(
        ApiError.UnauthorizedError(
          "No token in authorizationHeader after splitting"
        )
      );
    }

    const adminData = tokenService.validateAccessToken(token);
    if (!adminData) {
      return next(ApiError.UnauthorizedError("Token  didn't pass validation"));
    }
    req.user = adminData;
    next();
    return;
  } catch (error) {
    return next(ApiError.UnauthorizedError());
  }
};
export { authRequired };
