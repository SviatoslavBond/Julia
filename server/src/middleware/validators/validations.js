import { body, check, param, query } from "express-validator";
import { ALLOWED_CATEGORY_FIELDS } from "../../utils/allowedCategoryFields.js";
import { VALID_FIELDS_CREATE_STORY } from "../../utils/validationFields.js";
const storyCreateValidation = [
  body(
    VALID_FIELDS_CREATE_STORY.storyName.name,
    VALID_FIELDS_CREATE_STORY.storyName.msg
  )
    .isLength({ min: 4 })
    .isString(),
  body(
    VALID_FIELDS_CREATE_STORY.location.name,
    VALID_FIELDS_CREATE_STORY.location.msg
  ).isString(),
  body(
    VALID_FIELDS_CREATE_STORY.category.name,
    VALID_FIELDS_CREATE_STORY.category.msg
  )
    .isString()
    .isIn(ALLOWED_CATEGORY_FIELDS),
  body(
    VALID_FIELDS_CREATE_STORY.describe.name,
    VALID_FIELDS_CREATE_STORY.describe.msg
  ).isString(),
  body(
    VALID_FIELDS_CREATE_STORY.title.name,
    VALID_FIELDS_CREATE_STORY.title.msg
  ).isString(),
  body(
    VALID_FIELDS_CREATE_STORY.date.name,
    VALID_FIELDS_CREATE_STORY.date.msg
  ).isString(),
  body(
    VALID_FIELDS_CREATE_STORY.photoData.name,
    VALID_FIELDS_CREATE_STORY.photoData.msg
  ).isArray(),
];

const storyUpdateValidation = [
  body("storyName", "Enter storyName of story").isLength({ min: 4 }).isString(),
  body("location", "Enter location of story").isString(),
  body(
    "category",
    "Category is required. Field should contain one of those - wedding, content, family, engagement"
  ).custom((value) => {
    if (
      typeof value !== "object" ||
      !value.name ||
      typeof value.categoryOrder !== "number"
    ) {
      throw new Error(
        "Category must be an object with name and categoryOrder fields"
      );
    }
    return true;
  }),

  body("describe", "Describe is required").isString(),
  body("title", " Enter title of story").isString(),
  body("date", "You did not type the date").isString(),
  body("prefixPath", "Should be a string ").isString(),
  body("numberOfPhoto", "It should ne a number").isInt(),
  body("photo", "Should be an array").isArray(),
  body("_id", "Should be an mongoose ObjectId").isMongoId(),
  check(
    "photo.*.cover",
    "Every object in array photo should contain field - cover:boolean."
  ).isBoolean(),
  check(
    "photo.*.publicUrl",
    "Every object in array photo should contain field  publicUrl:url."
  ).isString(),
  // body().isObject()
];

const userCreateValidation = [
  body("email", "Email is required").isEmail(),
  body("password", "Password must be at least 8 characters").isLength({
    min: 8,
  }),
];
const getStoriesValidation = [
  query("category", (val) => {
    return {
      msg: `Value of query parametr - ${val} is invalid`,
      availibleValues: ALLOWED_CATEGORY_FIELDS,
    };
  })
    .isString()
    .optional()
    .isIn(ALLOWED_CATEGORY_FIELDS),
];
const mongoObjectIdValidationInParams = [
  param("id", "Invalid mongoose ObjectId, please try agian").isMongoId(),
];
export {
  storyCreateValidation,
  userCreateValidation,
  storyUpdateValidation,
  mongoObjectIdValidationInParams,
  getStoriesValidation,
};
