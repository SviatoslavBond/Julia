import { validationResult } from "express-validator";
import { ApiError } from "../../exeptions/api.error.js";
const handleValidationErrors = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    throw ApiError.BadRequest(
      "Validation hasn't passed. Try again",
      errors.errors
    );
  }
  next();
};
export default handleValidationErrors;
