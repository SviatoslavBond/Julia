import { ApiError } from "../../exeptions/api.error.js";
export function filesValidator(req, res, next) {
  if (!req.files || req.files.length === 0) {
    throw ApiError.BadRequest("No file, must be at least 1 file");
  }
  next();
}
