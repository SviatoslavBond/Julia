import multer from "multer";
import fs from "fs";
// DEPRICATED CODE
const storageConfig = multer.diskStorage({
  destination: (req, file, cb) => {
    const { categoryfolder, foldername } = req.body;

    if (!fs.existsSync(`public/${categoryfolder}/${foldername}`)) {
      fs.mkdirSync(`public/${categoryfolder}/${foldername}`);
    }
    cb(null, `public/${categoryfolder}/${foldername}`);
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});
// DEPRICATED CODE
const uploadImages = multer({ storage: storageConfig });

const multerGCS = multer({
  storage: multer.memoryStorage(),
  limits: {
    fileSize: 5 * 1024 * 1024,
  },
});

export { uploadImages, multerGCS };
