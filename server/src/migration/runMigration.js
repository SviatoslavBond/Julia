import { StoryModel } from "../models/story.js";
import { categoryNames } from "../utils/categoryName.js";
export const runMigration = async () => {
  try {
    categoryNames.forEach(async (cat, i) => {
      const stories = await StoryModel.find({ category: cat.value });
      if (stories.length === 0) {
        console.log(`No need migration in category -- ${cat.value}`);
        return;
      }
      stories.forEach(async (story, i) => {
        story.category = {
          name: cat.value,
          categoryOrder: i,
        };
        await story.save();
      });

      console.log(
        `Migration completed: ${stories.length} documents updated in category -- ${cat.value}`
      );
    });
  } catch (error) {
    console.error("Migration failed:", error);
  }
};
