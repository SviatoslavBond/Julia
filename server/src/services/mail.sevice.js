//@ts-check
import nodemailer from "nodemailer";
import { ApiError } from "../exeptions/api.error.js";

class MailSevice {
  constructor() {
    this.transporter = nodemailer.createTransport({
      // @ts-ignore
      host: process.env.SMTP_HOST,
      port: process.env.SMTP_PORT,
      secure: false,
      auth: {
        user: process.env.SMTP_USER,
        pass: process.env.SMTP_PASSWORD,
      },
    });
  }
  async sendActivationMail(email, link) {
    const info = await this.transporter.sendMail(
      {
        from: process.env.SMTP_USER,
        to: email,
        subject: `Activation account ${process.env.API_URL}`,
        text: "",
        html: `
      <div>
        <h1>To confirm registration follow  
        <a href="${link}">I want to finish registration</a>
        </h1>
      </div>
      `,
      },
      function (error, info) {
        if (error) {
          throw ApiError.BadRequest("No connection with mail", [error, info]);
        } else {
          console.log("Email sent: " + info.response);
        }
      }
    );
    return info;
  }
}
export const mailService = new MailSevice();
