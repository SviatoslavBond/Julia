import { StoryModel } from "../models/story.js";

class StoryEditService {
  static async addPhotoToStory(storyID, photos) {
    const updatedStory = await StoryModel.findByIdAndUpdate(
      { _id: storyID },
      {
        $push: {
          photo: { $each: photos },
        },
        $inc: {
          numberOfPhoto: +photos.length,
        },
      },
      { new: true }
    );
    return updatedStory;
  }

  static async updateStoryOrder(storyIDs) {
    const bulkOps = storyIDs.map((storyId, index) => ({
      updateOne: {
        filter: { _id: storyId },
        update: { "category.categoryOrder": index },
      },
    }));

    const bulkResult = await StoryModel.bulkWrite(bulkOps);

    return bulkResult;
  }

  static async deleteOneImageFromStory(storyID, filename) {
    const updatedStory = await StoryModel.findByIdAndUpdate(
      { _id: storyID },
      {
        $pull: {
          photo: { filename },
        },
        $inc: {
          numberOfPhoto: -1,
        },
      },
      { new: true }
    );

    return updatedStory;
  }
  /**
   * @typedef {Object} StoryType
   * @property {string} storyName - The name of the story.
   * @property {string} describe - The description of the story.
   * @property {string} category - The category of the story.
   * @property {string} location - The location of the story.
   * @property {string} date - The date of the story.
   * @property {string} title - The title of the story.
   * @property {string} prefixPath - The prefix path of the story.
   * @property {number} numberOfPhoto - The number of photos in the story.
   * @property {Array} photo - An array of photo objects.
   * @property {Date} createdAt - The creation date of the story.
   * @property {Date} updatedAt - The last update date of the story.
   * @property {number} __v - The version of the story.
   * @property {string} _id - The version of the story.
   */
  /**
   *
   * @param {string} storyId
   * @param {StoryType} newStory
   * @returns
   */
  static async updateOne(newStory) {
    const story = await StoryModel.findByIdAndUpdate(newStory._id, newStory, {
      new: true,
    });
    return story;
  }
}
export { StoryEditService };
