//@ts-check
import { UserModel } from "../models/user.model.js";
import { v4 as uid } from "uuid";
import bcrypt from "bcrypt";
import { mailService } from "./mail.sevice.js";
import { tokenService } from "./token.service.js";
import { ApiError } from "../exeptions/api.error.js";
import { AdminDto } from "../dtos/user.dto.js";

class UserSevice {
  async registration(email, password) {
    const admin = await UserModel.find();
    if (admin.length >= 1) {
      throw ApiError.BadRequest(
        "Only 1 admin can be registered. Please login using your email and password"
      );
    }
    const hash = await bcrypt.hash(password, 8);
    const activationLink = uid();
    const newAdmin = await UserModel.create({
      email,
      password: hash,
      activationLink,
    });
    const adminDto = new AdminDto(newAdmin);
    await mailService.sendActivationMail(
      email,
      `${process.env.API_URL}/api/auth/activate/${activationLink}`
    );

    const tokens = tokenService.generateTokens({ ...adminDto });
    await tokenService.saveRefreshToken(adminDto.id, tokens.refreshToken);
    return {
      user: { ...adminDto },
      ...tokens,
    };
  }
  async activateLink(activationLink) {
    const admin = await UserModel.findOne({ activationLink });
    if (!admin) {
      throw ApiError.BadRequest("Activation link is broken");
    }
    admin.isActivated = true;
    await admin.save();
  }

  async login(email, password) {
    const admin = await UserModel.findOne({ email });
    if (!admin) {
      throw ApiError.BadRequest(
        `Admin with ${email} email has not been founded`
      );
    }
    const isPassEquels = await bcrypt.compare(password, admin.password);
    if (!isPassEquels) {
      throw ApiError.BadRequest("Invalid credentials");
    }
    const adminDto = new AdminDto(admin);
    const tokens = tokenService.generateTokens({ ...adminDto });
    await tokenService.saveRefreshToken(adminDto.id, tokens.refreshToken);
    return {
      user: { ...adminDto },
      ...tokens,
    };
  }

  async logout(refreshToken) {
    const token = tokenService.removeToken(refreshToken);
    return token;
  }

  async refreshToken(refreshToken) {
    if (!refreshToken) {
      throw ApiError.UnauthorizedError("Refresh token was not passed");
    }
    const adminData = tokenService.validateRefreshToken(refreshToken);

    const tokenFromDB = await tokenService.findToken(refreshToken);

    if (!adminData || !tokenFromDB) {
      throw ApiError.UnauthorizedError("Error while refreshing token");
    }
    //@ts-ignore
    const admin = await UserModel.findById(adminData.id);
    const adminDto = new AdminDto(admin);
    const tokens = tokenService.generateTokens({ ...adminDto });

    await tokenService.saveRefreshToken(adminDto.id, tokens.refreshToken);
    return { ...tokens, user: adminDto };
  }
  async getAdmin() {
    const admin = await UserModel.find();
    return admin;
  }
}
export const userService = new UserSevice();
