import { fileService } from "./images.service.js";
import { StoryModel } from "../models/story.js";
import { v4 as uid } from "uuid";
import { ApiError } from "../exeptions/api.error.js";
class StoryService {
  async createStory(body, files) {
    const { category, photoData } = body;
    const prefixPath = `${category}/${uid()}/`;
    const savedPhoto = await fileService.uploadFiles(files, prefixPath);

    savedPhoto.images.forEach((photo, i) => {
      savedPhoto.images[i] = {
        ...photo,
        cover: photoData[i].cover,
      };
    });
    const numberOfPhoto = savedPhoto.images.length;
    const storiesByCategory = await StoryModel.countDocuments({
      "category.name": category,
    });
    const document = await StoryModel.create({
      ...body,
      category: {
        name: category,
        categoryOrder: storiesByCategory,
      },
      photo: savedPhoto.images,
      prefixPath,
      numberOfPhoto,
    });
    return document;
  }

  async deleteOne(id) {
    const story = await storyService.checkStoryExist(id);

    await fileService.deleteFiles(story.prefixPath);
    const deletingResult = await Promise.all(
      story.photo.map(({ filename }) => {
        return fileService.checkFileExist(filename);
      })
    );
    const isDeletingFilesSuccess = deletingResult.every((val) => val === false);
    if (!isDeletingFilesSuccess) {
      throw new Error("Attemp delete files was failed. try again");
    }

    return await StoryModel.deleteOne({ _id: id });
  }

  /**
   * @param {string} storyId Valid ObjectId string
   * @throws {ApiError.NotFound} If the story with the specified ID is not found.
   * @returns {Promise<StoryModel>} A Promise that resolves to the story document if it exists.
   */
  async checkStoryExist(storyId) {
    const story = await StoryModel.findById(storyId);
    if (!story) {
      throw ApiError.NotFound(`Story with ID ${storyId} wasn't found`);
    }
    return story;
  }
}
export const storyService = new StoryService();
