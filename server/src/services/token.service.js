//@ts-check
import jwt from "jsonwebtoken";
import { TokenModel } from "../models/token.model.js";

class TokenSevice {
  generateTokens(payload) {
    const accessToken = jwt.sign(
      payload,
      process.env.ACCESS_TOKEN_KEY || "SECRET_ACCESS_KEY",
      { expiresIn: process.env.LIFE_TIME_ACCESS_TOKEN }
    );
    const refreshToken = jwt.sign(
      payload,
      process.env.REFRESH_TOKEN_KEY || "SSECRET_REFRESH_KEY",
      { expiresIn: process.env.LIFE_TIME_REFRESH_TOKEN }
    );
    return { accessToken, refreshToken };
  }

  async saveRefreshToken(userId, refreshToken) {
    const token = await TokenModel.findOne({ user: userId });
    if (token) {
      token.refreshToken = refreshToken;
      return token.save();
    }
    const newToken = await TokenModel.create({ user: userId, refreshToken });
    return newToken;
  }

  async removeToken(refreshToken) {
    const tokenData = await TokenModel.deleteOne({ refreshToken });
    return tokenData;
  }

  async findToken(refreshToken) {
    const tokenData = await TokenModel.findOne({ refreshToken });
    return tokenData;
  }

  validateAccessToken(token) {
    try {
      const adminData = jwt.verify(
        token,
        process.env.ACCESS_TOKEN_KEY || "SECRET_ACCESS_KEY"
      );
      return adminData;
    } catch (error) {
      return null;
    }
  }

  validateRefreshToken(token) {
    try {
      const adminData = jwt.verify(
        token,
        process.env.REFRESH_TOKEN_KEY || "SECRET_REFRESH_KEY"
      );
      return adminData;
    } catch (error) {
      return null;
    }
  }
}
export const tokenService = new TokenSevice();
