import { bucket } from "../utils/clientGoogleStorage.js";
import { v4 as uid } from "uuid";
import pLimit from "p-limit";

class FileService {
  async uploadFiles(files, folder) {
    const limit = pLimit(5);
    const promises = [];
    files.forEach((file) => {
      promises.push(
        limit(() => {
          return uploadFileToGCS(file);
        })
      );
    });

    const uploadFileToGCS = (uploadedFile) => {
      return new Promise((resolve, reject) => {
        const gcsname = `${folder}${uid()}_${uploadedFile.originalname}`;
        const file = bucket.file(gcsname);
        const publicUrl = file.publicUrl();

        const stream = file.createWriteStream({
          metadata: {
            contentType: uploadedFile.mimetype,
            cacheControl: "no-cache",
          },
          resumable: false,
        });

        stream.on("error", (err) => {
          reject({
            msg: "This ocured while files were uploading to Google cloud. Try again please",
            err: err.message,
          });
        });

        stream.on("finish", () => {
          resolve({
            publicUrl,
            filename: gcsname,
            mimeType: uploadedFile.mimetype,
            originalName: uploadedFile.originalname,
            size: uploadedFile.size,
            cover: false,
          }); // data for client
        });
        stream.end(uploadedFile.buffer);
      });
    };

    const uploadedFiles = await Promise.all(promises);
    return { prefixPath: folder, images: uploadedFiles };
  }
  // This method doesn't throw error if files wasn't found under prefixpath
  async deleteFiles(prefixPath) {
    return await bucket.deleteFiles({
      prefix: prefixPath,
    });
  }
  // This method throw error if file wasn't found
  async deleteOneFile(filename) {
    const file = bucket.file(filename);
    await file.delete();
  }

  async checkFileExist(filename) {
    const file = bucket.file(filename);
    const result = await file.exists();
    return result[0];
  }

  async movePhoto(options) {
    const { image, category, prefixPath } = options;
    const file = bucket.file(image.filename);

    const movingPromise = new Promise((res, rej) => {
      file.move(
        `${category}/${prefixPath}/${uid()}_${image.originalName}`,
        function (err, destinationFile, apiResponse) {
          if (err) {
            return rej(err);
          }
          const newFileName = apiResponse.resource.name;
          res({
            ...image,
            filename: newFileName,
            publicUrl: `https://storage.googleapis.com/${process.env.GOOGLE_BUCKET_NAME}/${newFileName}`,
          });
        }
      );
    });
    const data = await movingPromise;

    return data;
  }
}
const fileService = new FileService();
export { fileService };
