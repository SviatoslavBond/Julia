export const REGISTRATION = "/api/auth/registration";
export const LOGIN = "/api/auth/login";
export const LOGOUT = "/api/auth/logout";
export const AUTH = "/api/auth";
export const ACTIVATE_LINK = "/api/auth/activate/:link";
export const REFRESH_TOKEN = "/api/auth/refresh";
