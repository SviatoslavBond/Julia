export const STORY = "/api/stories";
export const STORY_EDIT = "/api/edit/story";
export const UPDATE_STORY_ORDER = "/api/edit/update-order";
export const IMAGES_EDIT = "/api/images/";
