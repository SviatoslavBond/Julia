export const loadGoogleMapScript = {
  script: document.createElement("script"),
  onMount: function () {
    this.script.src =
      "https://maps.googleapis.com/maps/api/js?key=AIzaSyApZ4cztC7gJbenV4U6gsqVmenVUXyKvF0&libraries=places";
    this.script.async = true;
    this.script.defer = true;
    document.body.appendChild(this.script);
  },
  onUnmount: function () {
    document.body.removeChild(this.script);
  },
};
