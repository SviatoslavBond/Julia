export const formatDate = (date) => {
  const data = new Date(date);
  const hours = data.getUTCHours();
  const minutes = data.getUTCMinutes();
  const formattedHours = hours.toString().padStart(2, "0");
  const formattedMinutes = minutes.toString().padStart(2, "0");
  const timeString = `${formattedHours}:${formattedMinutes}`;

  const options = {
    weekday: "long",
    year: "numeric",
    month: "long",
    day: "numeric",
  };
  return `${data.toLocaleDateString("uk-UA", options)}${timeString}`;
};
