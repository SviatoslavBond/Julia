// This method cheks that object or array have elements end return boolean
export const hasElements = (obj) => {
  if (typeof obj !== "object" || obj === null || obj === undefined) {
    return false;
  }
  if (Array.isArray(obj)) {
    return obj.length > 0;
  }
  return Object.keys(obj).length > 0;
};
