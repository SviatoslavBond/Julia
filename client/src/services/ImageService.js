import { $api } from "axiosInstance";
import { IMAGES_EDIT } from "utils/ApiRoutesConstants/story";

class ImageService {
  static async deleteOneFoto(filename) {
    try {
      return await $api.delete(`${IMAGES_EDIT}`, { data: { filename } });
    } catch (error) {
      console.log(error);
      return null;
    }
  }
}
export { ImageService };
