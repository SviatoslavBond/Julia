import { useState } from "react";
import "./createStory.scss";
import { useParams } from "react-router-dom";
import { storyStore } from "store/StoryState";
import { observer } from "mobx-react-lite";
import { toast } from "react-toastify";
import { StoryService } from "services/StoryService";
import { FormStory } from "./FormStory";
import { inputNames } from "./inputNames";

const CreateStory = observer(() => {
  const { id } = useParams();
  const [loading, setLoading] = useState(false);

  // Функція для провірки чи є cover фото якщо немає то встановлюється перше фото як cover
  const setCoverDefault = (photos) => {
    const isCoverExist = photos.find((item) => item.cover === true);
    return !isCoverExist
      ? photos.map((item, i) => (i === 0 ? { ...item, cover: true } : item))
      : photos;
  };
  const addStory = async (form, photo, resetForm) => {
    try {
      setLoading(true);
      const photoData = photo.map((image) => ({
        cover: image.cover,
      }));

      const story = {
        storyName: form.storyname,
        category: form.category,
        location: form.location,
        describe: form.describe,
        title: form.title,
        date: form.date.replace(/\./g, "-"),
        photoData,
        photo: photo.map((image) => {
          return image.originalFile;
        }),
      };

      const data = await StoryService.createStory(story);

      if (data) {
        inputNames.resetLocalStorage();
        toast.success("История сохранена!!");
        setLoading(false);
        resetForm();
      } else {
        toast.error("Что то пошло не так(");
      }
    } catch (e) {
      console.log(e);
      setLoading(false);
      toast.error("Что то пошло не так(");
    }
  };

  const saveChanges = async (form, _, resetForm) => {
    try {
      const story = {
        ...storyStore.storyToChange,
        numberOfPhoto: form.photo.length,
        storyName: form.storyname,
        category: {
          name: form.category,
          categoryOrder: storyStore.getStoryToChange.category.categoryOrder,
        },
        location: form.location,
        describe: form.describe,
        title: form.title,
        date: form.date.replace(/\./g, "-"),
        photo: setCoverDefault(form.photo),
      };

      const data = await StoryService.editStory(story);
      if (data) {
        toast.success("История изменена!!");
        resetForm();
      } else {
        toast.error("История не изменена");
      }
    } catch (error) {
      console.log(error);
      toast.error("Что то пошло не так");
    }
  };

  return (
    <>
      {id ? (
        <FormStory
          key={"edit story"}
          loading={loading}
          onSubmit={saveChanges}
          id={id}
        />
      ) : (
        <FormStory key={"create story"} loading={loading} onSubmit={addStory} />
      )}
    </>
  );
});

export { CreateStory };
