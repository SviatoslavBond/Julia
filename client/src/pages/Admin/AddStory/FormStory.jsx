import React, { useEffect } from "react";
import SelectCity from "components/SelectCity/SelectCity";
import { storyCat } from "utils/storyCategories";
import DropZone from "../../../components/DropZone/DropZone";
import "./createStory.scss";
import { Card, Box, Button, Typography, Stack } from "@mui/material";
import { storyStore } from "store/StoryState";
import { observer } from "mobx-react-lite";
import { useForm, Controller } from "react-hook-form";
import EditPreviewer from "components/EditPreviewer/EditPreviewer";
import { toast } from "react-toastify";
import { Input, SelectInput } from "components/UI/ControlledTextField";
import { inputNames } from "./inputNames";
import { StoryService } from "services/StoryService";
import CircularProgress from "@mui/material/CircularProgress";
import clsx from "clsx";

export const FormStory = observer(({ onSubmit, id, loading }) => {
  const {
    handleSubmit,
    formState: { errors },
    reset,
    watch,
    clearErrors,
    setValue,
    control,
  } = useForm({
    defaultValues: {
      photo: [],
    },
  });
  const photo = watch("photo");
  useEffect(() => {
    const fetchData = async () => {
      try {
        storyStore.setIsEditable(true);
        storyStore.setStoryId(id);
        const { data } = await StoryService.getStoryById(id);
        storyStore.setStoryToChange(data);
        setValue("storyname", data.storyName);
        setValue("title", data.title);
        setValue("describe", data.describe);
        setValue("location", data.location);
        setValue("date", data.date.replace(/\./g, "-"));
        setValue("photo", data.photo);
        setValue("category", data.category.name);
      } catch (error) {
        console.error("Ошибка во время загрузки истории:", error);
        toast.error("Что то пошло не так(");
        resetToInitialState();
      }
    };
    if (id) {
      fetchData();
    } else {
      if (inputNames.isDataInLS) {
        inputNames.getInputNames.forEach((item) => {
          setValue(item, inputNames.getValueFromLS(item));
        });
      }
    }

    // reset on unmounting
    return () => {
      resetToInitialState();
    };
  }, [id]);

  const onSubmitWrapper = (form) => {
    onSubmit(form, photo, resetToInitialState);
  };
  const resetToInitialState = () => {
    reset();
    clearErrors();
    storyStore.resetState();
  };

  return (
    <Box sx={{ pb: "10px" }}>
      <Card className={clsx("addStory", { "addStory-loading": loading })}>
        <Box
          className={clsx("addStory_spinner", {
            "addStory_spinner-active": loading,
          })}
        >
          <CircularProgress
            className="addStory_spinner_progress"
            color="secondary"
          />
          <p className="addStory_spinner_text">Сохраняем историю...</p>
        </Box>
        {storyStore.isEditable ? (
          <Typography className="addStory__title">
            Редактировать историю
          </Typography>
        ) : (
          <Typography className="addStory__title">Загружаем историю</Typography>
        )}

        <form onSubmit={handleSubmit(onSubmitWrapper)}>
          <Input
            isEditable={!!id}
            control={control}
            name={inputNames.storyname}
            errorText="Имя слишком корткое"
            label="Story name"
          />
          <Input
            isEditable={!!id}
            control={control}
            name={inputNames.title}
            errorText="Заголовок обязателен"
            label="Title"
          />
          <Input
            isEditable={!!id}
            control={control}
            name={inputNames.describe}
            errorText="Описание должно быть"
            label="Description"
          />
          <Stack direction="row" justifyContent="center" alignItems="center">
            <Typography className="addStory__fieldText">Дата съемки</Typography>
            <Input
              isEditable={!!id}
              control={control}
              name={inputNames.date}
              errorText="Заголовок обязателен"
              label="Title"
              type="date"
            />
          </Stack>
          <Stack direction="row" justifyContent="center" alignItems="center">
            <Typography className="addStory__fieldText">Вид съемки</Typography>
            <SelectInput
              isEditable={!!id}
              errorText="Нужно выбрать вид съемки"
              control={control}
              name={inputNames.category}
              label="Вид съемки"
              items={storyCat}
            />
          </Stack>

          <SelectCity
            control={control}
            isEditable={!!id}
            error={!!errors.location}
            setValue={setValue}
          />
          <Controller
            name="photo"
            control={control}
            rules={{
              required: true,
              validate: (value) => {
                return value.length >= 10;
              },
            }}
            defaultValue={[]}
            render={() => (
              <DropZone
                error={errors.photo}
                photo={photo}
                setValue={setValue}
              />
            )}
          />
          <Box
            display={"flex"}
            alignItems={"center"}
            justifyContent={"center"}
            sx={{ marginBottom: 2 }}
          >
            {storyStore.isEditable ? (
              <Button
                variant="contained"
                sx={{ marginRight: 2 }}
                color="warning"
                type="submit"
              >
                Сохранить изменения
              </Button>
            ) : (
              <Button
                variant="contained"
                sx={{ marginRight: 2 }}
                color="warning"
                type="submit"
              >
                Сохранить историю
              </Button>
            )}
            <Button
              variant="contained"
              sx={{ marginRight: 2 }}
              color="error"
              type="button"
              onClick={() => resetToInitialState()}
            >
              Сбросить
            </Button>
          </Box>
        </form>
        <EditPreviewer setValue={setValue} photo={photo} />
      </Card>
    </Box>
  );
});
