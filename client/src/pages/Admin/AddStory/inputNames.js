const inputNames = {
  storyname: "storyname",
  title: "title",
  describe: "describe",
  location: "location",
  date: "date",
  category: "category",
  get length() {
    return Object.keys(this).length;
  },
  get getInputNames() {
    return Object.keys(this);
  },
  get isDataInLS() {
    return this.getInputNames.some((item) => {
      return localStorage.getItem(item);
    });
  },
  getValueFromLS(inputName) {
    const LSvalue = localStorage.getItem(inputName);
    return LSvalue ? LSvalue : "";
  },
  resetLocalStorage() {
    this.getInputNames.forEach((item) => {
      localStorage.removeItem(item);
    });
  },
};
Object.defineProperties(inputNames, {
  length: {
    enumerable: false,
  },
  getInputNames: {
    enumerable: false,
  },
  isDataInLS: {
    enumerable: false,
  },
  getValueFromLS: {
    enumerable: false,
  },
  resetLocalStorage: {
    enumerable: false,
  },
});
Object.freeze(inputNames);
export { inputNames };
