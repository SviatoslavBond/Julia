import React, { useEffect, useState } from "react";
import { Link, Routes, Route, NavLink, Navigate } from "react-router-dom";
import { PlusCircle } from "components/UI/icons";
import { AdminStoryList } from "pages/Admin/AdminStoryList/AdminStoryList";
import "./admin.scss";
import { Button } from "@mui/material";
import { observer } from "mobx-react-lite";
import { authStore } from "store/AuthStore";
import { LOGIN_PAGE } from "utils/RouterConstants/routerConstants";
import { CreateStory } from "./AddStory/CreateStory";
import { loadGoogleMapScript } from "utils/loadGoogleMapScript";
import {
  WEDDINGS,
  FAMILY,
  LOVESTORY,
  CONTENT,
  EDIT_STORY_BY_ID,
  ADD_STORY,
} from "utils/RouterConstants/routerConstants";

const Admin = observer(() => {
  const [loadScript, setLoadScript] = useState(true);
  const activeLink = ({ isActive }) =>
    isActive ? { color: "orange" } : { color: "white" };
  if (loadScript) {
    loadGoogleMapScript.onMount();
    setLoadScript(false);
  }

  useEffect(() => {
    return () => {
      loadGoogleMapScript.onUnmount();
    };
  }, []);

  if (!authStore.isAuth) {
    return <Navigate to={LOGIN_PAGE} />;
  }
  return (
    <div className="admin">
      <div className="admin__logo">
        <Link to="/">
          <h2 className="admin__title">Julia Kulyok</h2>
        </Link>
        <Button
          variant="contained"
          color="warning"
          onClick={() => authStore.logout()}
        >
          Log out
        </Button>
      </div>

      <div className="admin__inner">
        <aside className="admin__aside">
          <h3 className="admin__side-title">My stories</h3>
          <div className="admin__menu">
            <ul className="admin__menu-list">
              <li className="admin__menu-item">
                <NavLink style={activeLink} to={WEDDINGS}>
                  Weddings
                </NavLink>
              </li>
              <li className="admin__menu-item">
                <NavLink style={activeLink} to={LOVESTORY}>
                  Love story
                </NavLink>
              </li>
              <li className="admin__menu-item">
                <NavLink style={activeLink} to={FAMILY}>
                  Family
                </NavLink>
              </li>
              <li className="admin__menu-item">
                <NavLink style={activeLink} to={CONTENT}>
                  Content
                </NavLink>
              </li>
              <li className="admin__menu-item">
                <NavLink style={activeLink} to="all-stories">
                  All stories
                </NavLink>
              </li>
            </ul>
            <Link to={ADD_STORY} className="admin__addStory">
              <div className="admin__addStoryText">Create new story</div>
              <PlusCircle className="admin__addStoryIcon" />
            </Link>
          </div>
        </aside>

        <div className="admin__main">
          <Routes>
            <Route path={ADD_STORY} element={<CreateStory />} />
            <Route path={EDIT_STORY_BY_ID} element={<CreateStory />} />
            <Route path={WEDDINGS} element={<AdminStoryList />} />
            <Route path={LOVESTORY} element={<AdminStoryList />} />
            <Route path={CONTENT} element={<AdminStoryList />} />
            <Route path={FAMILY} element={<AdminStoryList />} />
            <Route path="all-stories" element={<AdminStoryList />} />
          </Routes>
        </div>
      </div>
    </div>
  );
});

export default Admin;
