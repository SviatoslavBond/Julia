import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Box } from "@mui/material";
import { AdminStoryCard } from "../../../components/AdminStoryCard/AdminStoryCard";
import { StoryService } from "services/StoryService";
import {
  GridContextProvider,
  GridDropZone,
  GridItem,
  swap,
} from "react-grid-dnd";
import { toast } from "react-toastify";

export const AdminStoryList = () => {
  const [stories, setStories] = useState([]);
  const path = useParams();
  const category = path["*"];
  useEffect(() => {
    async function fetchStories() {
      const response = await StoryService.getStoryByCategory(category);
      if (!response) {
        return;
      }
      setStories(response.data);
    }
    fetchStories();
  }, [path]);

  const onChange = async (__, sourceIndex, targetIndex) => {
    if (!sourceIndex || !sourceIndex) {
      return;
    }
    const nextState = swap(stories, sourceIndex, targetIndex);
    const modified = nextState.map((story, i) => {
      return {
        ...story,
        category: {
          name: story.category.name,
          categoryOrder: i,
        },
      };
    });
    try {
      const storyIDs = modified.map((story) => story._id);
      const response = await StoryService.updateStoryOrder(storyIDs);
      if (response) {
        setStories(modified);
        toast.success("Порядок изменен");
      }
    } catch (error) {
      toast.error("Ошыбка при изменении порядка историй");
    }
  };

  return (
    <Box>
      <GridContextProvider onChange={onChange}>
        <GridDropZone
          id="stories"
          boxesPerRow={1}
          rowHeight={320}
          style={{
            height: 1900,
          }}
        >
          {stories.map((story) => {
            return (
              <GridItem key={story._id}>
                <AdminStoryCard setStories={setStories} story={story} />
              </GridItem>
            );
          })}
        </GridDropZone>
      </GridContextProvider>
    </Box>
  );
};
