import { useState, useEffect } from "react";
import style from "./auth.module.scss";
import { TextField, Card, Button, Alert, Box } from "@mui/material";
import { AuthService } from "services/AuthService";
import { Link, Navigate } from "react-router-dom";
import { observer } from "mobx-react-lite";
import { authStore } from "store/AuthStore";
import { HOME } from "../../utils/RouterConstants/routerConstants";

export default observer(function Auth() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [adminIsRegistered, setadminIsRegistered] = useState(false);

  useEffect(() => {
    async function fetchAdminData() {
      const { data } = await AuthService.getAdmin();
      const adminExist = data.length > 0;
      setadminIsRegistered(adminExist);
    }
    fetchAdminData();
  }, []);

  const submit = async (e) => {
    e.preventDefault();
    adminIsRegistered
      ? await authStore.loginAdmin(email, password)
      : await authStore.registrationAdmin(email, password);

    if (authStore.error) {
      return;
    }
    setEmail("");
    setPassword("");
  };

  if (authStore.isAuth) {
    return <Navigate to="/admin" />;
  }

  return (
    <Box className={style.login}>
      <div className={style.login__inner}>
        <Link to={HOME}>
          <h2 className={style.login__logo}>Julia Kulyok</h2>
        </Link>

        <form onSubmit={submit}>
          <Card className={style.login__card}>
            <h1 className={style.login__title}>
              {adminIsRegistered ? "Log in as admin" : "Registration as admin"}
            </h1>

            <TextField
              margin="normal"
              label="Email"
              type="email"
              value={email}
              onChange={(e) => setEmail(e.currentTarget.value)}
              autoComplete="email"
            />
            <TextField
              margin="normal"
              id="outlined-password-input"
              label="Password"
              type="password"
              value={password}
              onChange={(e) => setPassword(e.currentTarget.value)}
              autoComplete="current-password"
            />
            <Box marginTop={3}>
              {authStore.error && (
                <Alert sx={{ marginBottom: "20px" }} severity="error">
                  Password or email is invalid
                </Alert>
              )}

              {!authStore.isAuth && (
                <Button variant="contained" type="submit">
                  {adminIsRegistered ? "Log in" : "Create admin"}
                </Button>
              )}
            </Box>
          </Card>
        </form>
      </div>
    </Box>
  );
});
