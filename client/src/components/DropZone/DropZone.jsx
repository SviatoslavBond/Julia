import React, { useState } from "react";
import clsx from "clsx";
import "./drop-zone.scss";
import { storyStore } from "store/StoryState";
import { observer } from "mobx-react-lite";
import { StoryService } from "services/StoryService";
import { Button } from "@mui/material";
import { toast } from "react-toastify";

const DropZone = observer(({ setValue, photo, error }) => {
  const [drag, setDrag] = useState(false);

  const addPhotoForCreatingStory = (filesFromInput) => {
    const paths = [...photo];
    for (let i = 0; i < filesFromInput.length; i++) {
      const url = URL.createObjectURL(filesFromInput[i]);
      paths.push({
        originalFile: filesFromInput[i],
        publicUrl: url,
        mimeType: filesFromInput[i].type,
        filename: null,
        originalName: filesFromInput[i].name,
        size: filesFromInput[i].size,
        cover: i === 0 ? true : false,
      });
    }
    setValue("photo", paths, { shouldValidate: true });
  };

  const addPhotoToExistingStory = async (filesFromInput) => {
    const photo = [];
    for (let i = 0; i < filesFromInput.length; i++) {
      photo.push(filesFromInput[i]);
    }
    const result = await StoryService.addPhotoToExistingStory(
      storyStore.storyId,
      { folder: storyStore.storyToChange.prefixPath, photo }
    );
    return result;
  };

  const handleUploadsImages = async (e) => {
    e.preventDefault();
    try {
      // get files from input
      const files = e.type === "change" ? e.target.files : e.dataTransfer.files;

      if (storyStore.isEditable && storyStore.storyId) {
        const updatedStory = await addPhotoToExistingStory(files);
        if (!updatedStory) {
          toast.error("Что то пошло не так");
          return;
        }

        toast.success("Успешное добавление фото");
        setValue("photo", updatedStory.data.photo, { shouldValidate: true });
      } else {
        addPhotoForCreatingStory(files);
      }
    } catch (error) {
      toast.error("Что то пошло не так");
    }
  };
  return (
    <div className="drop">
      <div
        onDragOver={(e) => {
          e.preventDefault();
          setDrag(true);
        }}
        onDragLeave={(e) => {
          e.preventDefault();
          setDrag(false);
        }}
        onDrop={handleUploadsImages}
        className={clsx("drop__inner", {
          "drop__inner--active": drag,
          "drop__inner--error": error,
        })}
      >
        {error ? (
          <div className="drop__text">Нужно загрузить не меньше 10 фото</div>
        ) : (
          <div className="drop__text">Вы загрузили {photo.length} фото</div>
        )}

        <div className="button__upload">
          <input
            id="uploads"
            onChange={handleUploadsImages}
            type="file"
            name="images"
            multiple="multiple"
          />

          <Button type="button" variant="contained">
            <label htmlFor="uploads">Обзор </label>
          </Button>
        </div>
      </div>
    </div>
  );
});

export default DropZone;
