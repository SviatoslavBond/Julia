import React, { useState } from "react";
import { MdOutlineLocationSearching } from "react-icons/md";
import clsx from "clsx";
import PlacesAutocomplete from "react-places-autocomplete";
import { Input } from "components/UI/ControlledTextField";
import { inputNames } from "pages/Admin/AddStory/inputNames";
import "./selectCity.scss";

const SelectCity = ({ setValue, isEditable, control }) => {
  const [address, setAdress] = useState("");

  const onSelect = (adress) => {
    setValue("location", adress, { shouldValidate: true });
    setAdress(adress);
  };
  const autoOnChange = (value) => {
    setValue("location", value);
    setAdress(value);
  };
  return (
    <PlacesAutocomplete
      value={address}
      onChange={autoOnChange}
      onSelect={onSelect}
      debounce={500}
    >
      {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
        <div className="sc">
          <MdOutlineLocationSearching className="sc__iconTarget" />
          <Input
            control={control}
            isEditable={isEditable}
            name={inputNames.location}
            label="Location"
            errorText="Локация обязательна"
            {...getInputProps({})}
          />

          <div className={clsx({ sg: suggestions.length !== 0 })}>
            {loading && <div>Loading...</div>}
            {suggestions.map((suggestion, i) => {
              const className = suggestion.active
                ? "sg__item sg__item--active"
                : "sg__item";
              return (
                <div
                  key={i}
                  {...getSuggestionItemProps(suggestion, {
                    className,
                  })}
                >
                  <span>{suggestion.description}</span>
                </div>
              );
            })}
          </div>
        </div>
      )}
    </PlacesAutocomplete>
  );
};

export default SelectCity;
