import {
  GridContextProvider,
  GridDropZone,
  GridItem,
  swap,
} from "react-grid-dnd";
import PreviewItem from "components/EditPreviewer/PreviewItem/PreviewItem";
import { StoryService } from "services/StoryService";
import { storyStore } from "store/StoryState";
import { observer } from "mobx-react-lite";
import "./editPreviewer.scss";
import { toast } from "react-toastify";

const EditPreviewer = observer(
  ({ countColumn = 5, heightRow = 150, setValue, photo }) => {
    const setCoverImage = (publicUrl) => {
      setValue(
        "photo",
        photo.map((el) =>
          el.publicUrl === publicUrl
            ? { ...el, cover: true }
            : {
                ...el,
                cover: false,
              }
        )
      );
    };

    const onChange = (__, sourceIndex, targetIndex) => {
      const nextState = swap(photo, sourceIndex, targetIndex);
      setValue("photo", nextState);
    };

    const deleteImage = async (publicUrl, filename) => {
      try {
        if (storyStore.isEditable && filename && storyStore.storyId) {
          const result = await StoryService.deleteOneFoto(
            storyStore.storyId,
            filename
          );
          if (!result) {
            //Notification
            toast.error("Попытка удалить фото неудачная");
            return;
          }
          // If deleting was successfully UI is being updated
          setValue(
            "photo",
            photo.filter((el) => el.publicUrl !== publicUrl)
          );
          //Notification
          toast.success("Успешное удаления фото");
          return;
        }
        setValue(
          "photo",
          photo.filter((el) => el.publicUrl !== publicUrl)
        );
      } catch (error) {
        //Notification
        toast.error("Попытка удалить фото неудачная");
        console.log(error);
      }
    };
    return (
      <GridContextProvider onChange={onChange}>
        <GridDropZone
          className="dropzone "
          id="photos"
          boxesPerRow={countColumn}
          rowHeight={heightRow}
          style={{
            height: heightRow * Math.ceil(photo.length / countColumn),
          }}
        >
          {photo.map((foto) => {
            return (
              <GridItem className="grid-item" key={foto.publicUrl}>
                <PreviewItem
                  deleteImage={deleteImage}
                  foto={foto}
                  setCoverImage={setCoverImage}
                />
              </GridItem>
            );
          })}
        </GridDropZone>
      </GridContextProvider>
    );
  }
);

export default EditPreviewer;
