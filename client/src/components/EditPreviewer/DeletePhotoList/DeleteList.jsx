import React from "react";
import { storyStore } from "store/StoryState";
import { observer } from "mobx-react-lite";
import { CiUndo } from "react-icons/ci";
import {
  Card,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
  IconButton,
  ListItemAvatar,
  Avatar,
  Divider,
  Box,
  ListSubheader,
  Tooltip,
} from "@mui/material";
import { MdDelete } from "react-icons/md";

export const DeleteList = observer(({ setValue, photo }) => {
  const handleDelete = (id) => {
    console.log(id);
  };
  const cancelDelete = (foto) => {
    setValue("photo", [...photo, foto]);
    storyStore.setDeletePhotoList((list) => {
      return list.filter((item) => item.publicUrl !== foto.publicUrl);
    });
  };
  if (storyStore.deletePhotoList.length === 0) {
    return null;
  }
  return (
    <List
      sx={{
        border: "2px red solid",
        borderRadius: "5px",
        padding: "20px",
        marginY: "20px",
      }}
      subheader={<ListSubheader>Список фото для удаления</ListSubheader>}
    >
      {storyStore.deletePhotoList.map((photo, index) => (
        <Box key={photo.publicUrl}>
          <ListItem disablePadding>
            <ListItemIcon>{index + 1}</ListItemIcon>
            <ListItemAvatar>
              <Avatar src={photo.publicUrl} />
            </ListItemAvatar>
            <ListItemText primary={photo.originalName} />
            <Tooltip title={"Удалить навсегда"}>
              <IconButton
                edge="end"
                sx={{ marginRight: "10px" }}
                aria-label="delete"
                onClick={() => handleDelete(photo._id)}
              >
                <MdDelete />
              </IconButton>
            </Tooltip>
            <IconButton
              edge="end"
              aria-label="cancel"
              onClick={() => cancelDelete(photo)}
            >
              <CiUndo />
            </IconButton>
          </ListItem>
          <Divider sx={{ marginY: "10px" }} variant="inset" component="li" />
        </Box>
      ))}
    </List>
  );
});
