import React from "react";
import { MdDelete } from "react-icons/md";
import IconCover from "../../UI/IconCover/IconCover";
import clsx from "clsx";
import "./previewItem.scss";
const PreviewItem = ({ foto, setCoverImage, deleteImage }) => {
  const { publicUrl, filename } = foto;

  return (
    // "up-item__cover" стилі для cover зображення
    <div className={clsx(["up-item"], { ["up-item__cover"]: foto.cover })}>
      <div className="up-block">
        {foto.cover ? null : (
          <div className="cover-button up-btn">
            <IconCover
              onClick={() => setCoverImage(publicUrl)}
              className="up-icons up-icons-cover"
            />
            <span>Обложка</span>
          </div>
        )}
        <div className="delete-button up-btn">
          <MdDelete
            onClick={() => deleteImage(publicUrl, filename)}
            className="up-icons  up-icons-delete"
          />
        </div>
      </div>
      <div className="opacity"></div>
      <div className="up-photo">
        <img src={publicUrl} alt="" />
      </div>
    </div>
  );
};

export default PreviewItem;
