import { useState } from "react";
import { Box, Paper, Typography, Button } from "@mui/material";
import { formatDate } from "utils/formateDate";
import { ImLocation2 } from "react-icons/im";
import {
  MdDriveFileRenameOutline,
  MdOutlineDescription,
  MdTitle,
  MdDateRange,
} from "react-icons/md";
import { StoryService } from "services/StoryService";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { useNavigate } from "react-router-dom";
import Popup from "components/Popup/Popup";
import { CautionDeleteStory } from "./CautionDeleteStory";
import "./storyCard.scss";

export const AdminStoryCard = ({ story, setStories }) => {
  const [showModal, setShowModal] = useState(false);
  const closeModal = () => setShowModal(false);
  const openModal = () => setShowModal(true);
  const {
    storyName,
    location,
    describe,
    title,
    createdAt,
    updatedAt,
    photo,
    _id,
    date,
  } = story;
  const coverPhoto = photo.find((photo) => photo.cover === true);
  const navigate = useNavigate();
  const deleteStory = async (id) => {
    const result = await StoryService.deletStoryById(id);
    if (!result) {
      toast.error("Error has occured while deleting");
      return;
    }
    setStories((state) => {
      return state.filter((item) => item._id !== id);
    });
    toast.success("Story was deleted.");
  };

  return (
    <Paper className="story-card" sx={{ marginY: 2, p: 2 }} elevation={3}>
      <Popup
        open={showModal}
        onClose={closeModal}
        content={
          <CautionDeleteStory
            deleteStory={() => deleteStory(_id)}
            close={closeModal}
          />
        }
      />
      <Box display={"flex"}>
        <Box sx={{ width: 150, maxHeight: 150, marginRight: 2 }}>
          <img
            alt="cover story"
            className="responseImg"
            src={coverPhoto ? coverPhoto.publicUrl : photo[0].publicUrl}
          />
          <Typography sx={{ mt: 2 }}>
            Количество фото: <b>{photo.length}</b>
          </Typography>
        </Box>
        <Box>
          <Typography sx={{ fontSize: 20, fontWeight: 700 }}>Info:</Typography>
          <Typography className="story-card__text">
            <ImLocation2 color="orange" /> <b>Location:</b> {location}
          </Typography>
          <Typography className="story-card__text">
            <MdDriveFileRenameOutline color="purple" /> <b>Name:</b> {storyName}
          </Typography>
          <Typography className="story-card__text">
            <MdOutlineDescription color="red" /> <b>Description:</b> {describe}
          </Typography>
          <Typography className="story-card__text">
            <MdTitle color="orange" /> <b>Title:</b> {title}
          </Typography>
          <Typography className="story-card__text">
            <MdDateRange color="green" /> <b>Created at:</b>{" "}
            {formatDate(createdAt)}
          </Typography>
          <Typography className="story-card__text">
            <MdDateRange color="green" /> <b>Updated at:</b>{" "}
            {formatDate(updatedAt)}
          </Typography>
          <Typography className="story-card__text">
            <MdDateRange color="green" /> <b>Дата сьемки</b> {date}
          </Typography>
          <Box sx={{ mt: 2 }}>
            <Button
              onClick={() => navigate(`/admin/edit/story/${_id}`)}
              variant="contained"
              sx={{ mr: 2 }}
            >
              Edit story
            </Button>
          </Box>
        </Box>
        <Box sx={{ ml: "auto", mt: "auto" }}>
          <Button onClick={() => openModal()} variant="contained" color="error">
            Delete story
          </Button>
        </Box>
      </Box>
    </Paper>
  );
};
