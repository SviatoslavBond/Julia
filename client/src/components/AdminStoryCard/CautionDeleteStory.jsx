import { useRef, useState } from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
export const CautionDeleteStory = ({ close, deleteStory }) => {
  const inputRef = useRef();
  const [error, setError] = useState("");
  const onClose = () => {
    setError("");
    close();
  };
  return (
    <form
      onSubmit={(event) => {
        event.preventDefault();
        const inputValue = inputRef.current.value;
        if (inputValue !== "DELETE") {
          setError("Введите 'DELETE' для подтверждения удаления");
          return;
        } else {
          deleteStory();
          setError("");
          close();
        }
      }}
    >
      <DialogTitle>Безвозвратное удаление истории</DialogTitle>
      <DialogContent>
        <DialogContentText>
          Вы хотите удалить историю. Подтвердите удаление - введите в поле снизу
          "<span style={{ color: "red" }}>DELETE</span>"
        </DialogContentText>
        <TextField
          inputRef={inputRef}
          onChange={(e) => {
            const value = e.target.value;
            if (value === "DELETE") {
              setError("");
            }
          }}
          autoFocus
          required
          name="delete"
          margin="dense"
          label="DELETE"
          type="text"
          fullWidth
          variant="outlined"
          error={!!error}
          helperText={error}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>Cancel</Button>
        <Button type="submit" color="warning">
          Confirm
        </Button>
      </DialogActions>
    </form>
  );
};
