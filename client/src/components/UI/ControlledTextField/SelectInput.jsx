import { TextField, MenuItem } from "@mui/material";
import { useController } from "react-hook-form";

export default function SelectInput({
  control,
  name,
  label,
  items,
  errorText,
  isEditable,
}) {
  const {
    field,
    fieldState: { error },
  } = useController({
    name,
    control,
    rules: { required: true },
  });
  if (!isEditable) {
    if (field.value !== undefined) {
      localStorage.setItem(name, field.value);
    }
    if (field.value === "") {
      localStorage.removeItem(name);
    }
  }
  return (
    <TextField
      error={!!error}
      helperText={!!error && errorText}
      fullWidth
      margin="normal"
      select
      variant="outlined"
      label={label}
      onChange={field.onChange}
      onBlur={field.onBlur}
      value={field.value ? field.value : ""}
      name={field.name}
      inputRef={field.ref}
    >
      {items.map((cat) => (
        <MenuItem key={cat.value} value={cat.value}>
          {cat.label}
        </MenuItem>
      ))}
    </TextField>
  );
}
