import { TextField } from "@mui/material";
import { useController } from "react-hook-form";

export default function Input({
  control,
  name,
  label,
  errorText,
  type = "text",
  isEditable,
  ...props
}) {
  const {
    field,
    fieldState: { error },
  } = useController({
    name,
    control,
    rules: { required: true },
  });
  const labelText = type === "text" ? label : "";

  if (!isEditable) {
    if (field.value !== undefined) {
      localStorage.setItem(name, field.value);
    }
    if (field.value === "") {
      localStorage.removeItem(name);
    }
  }
  return (
    <TextField
      error={!!error}
      helperText={!!error && errorText}
      fullWidth
      margin="normal"
      type={type}
      variant="outlined"
      label={labelText}
      onChange={field.onChange} // send value to hook form
      onBlur={field.onBlur} // notify when input is touched/blur
      {...props}
      value={field.value ? field.value : ""} // input value
      name={field.name} // send down the input name
      inputRef={field.ref} // send input ref, so we can focus on input when error appear
    />
  );
}
