import { makeAutoObservable, toJS } from "mobx";
class FetchedStories {
  _stories = [];
  _singleStory = {};
  constructor() {
    makeAutoObservable(this);
  }
  get stories() {
    return toJS(this._stories);
  }
  get singleStory() {
    return toJS(this._singleStory);
  }
  set stories(stories) {
    this._stories = stories;
  }
  set singleStory(story) {
    this._singleStory = story;
  }

  resetStore() {
    this._stories = [];
    this._singleStory = {};
  }
}
const fetchedStories = new FetchedStories();
export { fetchedStories };
