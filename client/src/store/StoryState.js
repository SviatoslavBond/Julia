import { makeAutoObservable, toJS } from "mobx";

class StoryState {
  loaded = false;
  isEditable = false;
  storyId = undefined;
  storyToChange = {
    storyName: "",
    location: "",
    date: "",
    photo: [],
    title: "",
    describe: "",
    prefixPath: "",
    category: {
      name: "",
      categoryOrder: 0,
    },
  };
  // unused
  deletePhotoList = [];

  constructor() {
    makeAutoObservable(this);
  }

  setLoaded(bool) {
    this.loaded = bool;
  }
  setIsEditable(bool) {
    this.isEditable = bool;
  }
  setStoryId(string) {
    this.storyId = string;
  }
  setStoryToChange(story) {
    this.storyToChange = story;
  }
  get getStoryToChange() {
    return toJS(this.storyToChange);
  }
  // unused
  setDeletePhotoList(value) {
    if (typeof value === "function") {
      this.deletePhotoList = value(this.deletePhotoList);
      return;
    }
    this.deletePhotoList.push(value);
  }

  resetState() {
    this.storyToChange = {
      storyName: "",
      location: "",
      date: "",
      photo: [],
      title: "",
      describe: "",
      prefixPath: "",
    };
    this.storyId = undefined;
    this.loaded = false;
    this.isEditable = false;
  }
}
const storyStore = new StoryState();
export { storyStore };
