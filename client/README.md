### Setting HOST for npm start

While using `npm start`, ensure to set the HOST variable to your current IP address.

#### Obtaining IP Address on Mac

To get your current IP address on a Mac, follow these steps:

1. Open Terminal.
2. Type the following command and press Enter:

   ```
   ifconfig
   ```

3. Look for your active network interface (usually labeled as "en0" or "en1").
4. Find the "inet" section under your active network interface. Your IP address will be listed next to "inet".
5. Replace the HOST value in your `npm start` command with your current IP address.

#### Example Usage

```bash
HOST=your_current_IP npm start

```

These steps give you ability using the phone via IP address and observe changes
