const express = require("express");
const fallback = require("express-history-api-fallback");
const compression = require("compression");
// const { createProxyMiddleware } = require("http-proxy-middleware");
const app = express();

app.use(compression());
console.log(process.env.PROXY);

app.use(express.static(`${__dirname}/build`));
// app.use(
//   "/api",
//   createProxyMiddleware({ target: process.env.PROXY, changeOrigin: true })
// );
app.use(fallback(`${__dirname}/build/index.html`));

app.listen(process.env.PORT || 3000);
